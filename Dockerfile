FROM openjdk:alpine

RUN mkdir /minecraft

COPY spigot.jar /minecraft/spigot.jar
COPY craftbukkit.jar /minecraft/craftbukkit.jar

COPY spigot/**/*.* /minecraft

EXPOSE 25565

ENTRYPOINT ["/usr/local/bin/spigot"]
CMD ["run"]