const https = require('https')
const fs = require('fs')
const path = require('path')

const mkdirp = require('mkdirp')

const Promise = require('bluebird')
const assert = require('assert')

const async = require('async')

const axios = require('axios')
const program = require('commander')

const agent = "Spigot Plugin Downloader CLI"

const apiURL = 'https://api.spiget.org/v2'
axios.defaults.baseURL = apiURL

program
  .version('0.0.1')
  .description('Spigot plugin downloader CLI')

program
  .command('download <ids...>')
  .alias('d')
  .option('-n, --names', 'Search for plugins by name and download the closest match')
  .option('-o, --output', 'Directory to output the plugins to, will be created if needed')
  .action(function (ids, cmd) {
    const output = cmd.output || './plugins/'
    mkdirp.sync(output)

    console.log(ids)
    if (cmd.names) {

    } else {
      console.log('Downloading by id..')
      async.mapLimit(ids, 5, function (id, callback) {
        const pluginpath = path.join(output, `${id}.jar`)
        const file = fs.createWriteStream(pluginpath)
        const url = apiURL + `/resources/${id}/download`
        console.log(' - ' + url)
        var request = https.get(url, function(response) {
          response.pipe(file).on('finish', function () {
            callback(null, pluginpath)
          })
        })
      }, (err, results) => {
        if (err) throw err
        // results is now an array of the response bodies
        console.log(results)
      })
    }
  })

program.parse(process.argv)